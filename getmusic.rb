#!/usr/bin/env ruby
require 'net/http'
require 'uri'
require 'rexml/document'
require 'xmmsclient'
require 'cgi'

include REXML

TRACKS = 2
USERNAME = 'memphis-stereo'
XMMS2_SERVER = 'tcp://sandpit.mcs.vuw.ac.nz:9876'
DOWNLOAD_DIR = '/media/music/getmusic'

def queryquote(string)
	string.gsub('<', '\<').gsub('>', '\>')
end

def fetch(uri_str, filename, limit = 10)
	# TODO: choose better exception
	raise ArgumentError, 'HTTP redirect too deep' if limit == 0
	
	response = Net::HTTP.get_response(URI.parse(uri_str))
	case response
	when Net::HTTPSuccess
			open(filename, 'wb') do |file|
				puts "writing to #{filename}..."
				file.write(response.body)
			end
	when Net::HTTPRedirection
		fetch(response['location'], filename, limit - 1)
	else
		response.error!
	end
end

rss = Net::HTTP.get(URI.parse("http://ws.audioscrobbler.com/2.0/user/#{USERNAME}/podcast.rss"))
xmldoc = Document.new(rss)

xmms = Xmms::Client.new('getmusic')
xmms.connect(XMMS2_SERVER)

downloaded = 0
xmldoc.elements.each('rss/channel/item') do |item|
	break if downloaded >= TRACKS
	
	title = item.elements['title'].text
	artist = item.elements['itunes:author'].text
	url = item.elements['enclosure'].attributes['url']
	filename = CGI.unescape(url[url.rindex('/') + 1..-1])
	
	#Check whether we already have this song
	query = Xmms::Collection.parse("artist:#{queryquote(artist)} title:#{queryquote(title)}")
	matches = xmms.coll_query_info(query, ['artist', 'album', 'title', 'id']).wait.value
	matches.each do |row|
		puts row.inspect
	end
	
	if matches.length == 0 && !File.exists?("#{DOWNLOAD_DIR}/#{filename}")
		puts "Downloading #{artist}: #{title} from #{url}"
		
		fetch(url, "#{DOWNLOAD_DIR}/#{filename}")
		
		downloaded += 1
		
		#Tell XMMS2 about it
		puts "adding file://#{DOWNLOAD_DIR}/#{filename}"
		puts xmms.medialib_add_entry("file://#{DOWNLOAD_DIR}/#{filename}").wait.value.inspect
	else
		puts "Not downloading #{artist}: #{title}, as it exists or we have #{matches.length} matches already"
	end
end

puts "Downloaded #{downloaded} tracks"
